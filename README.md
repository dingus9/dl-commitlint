# dl-commitlint

Builds docker container image containing the `commitlint` tool.

## Purpose

The Docker container built from this repository can be used to perform linting of commit messages based on the AngularJS commit standard located here: [Angular Style commit messages](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines)

## Usage

### CommitLint

The `commitlint` tool can be used to perform linting on commit messages. The linting rules can be configured in a `commitlint.config.js` file in the root of your repository. This file is mandatory.

This is the default `commitlint.config.js` file which is suggested.

```json
module.exports = {
        rules: {
                'body-leading-blank': [1, 'always'],
                'footer-leading-blank': [1, 'always'],
                'header-max-length': [2, 'always', 72],
                'scope-case': [2, 'always', 'lower-case'],
                'subject-case': [
                        0,
                        'never',
                        ['sentence-case', 'start-case', 'pascal-case', 'upper-case']
                ],
                'subject-empty': [2, 'never'],
                'subject-full-stop': [2, 'never', '.'],
                'type-case': [2, 'always', 'lower-case'],
                'type-empty': [2, 'never'],
                'type-enum': [
                        2,
                        'always',
                        [
                                'build',
                                'chore',
                                'ci',
                                'docs',
                                'feat',
                                'fix',
                                'perf',
                                'refactor',
                                'revert',
                                'style',
                                'test'
                        ]
                ]
        }
};
```

Below in an example of a `.gitlab-ci.yml` configuration file which enables `commitlint` on the repository.

```yml
commitchecker:
  image: registry.gitlab.com/dreamer-labs/dl-commitlint:latest
  stage: lint
  only:
    - merge_requests
  script:
    - git fetch origin ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME};
    - git fetch origin ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME};
    - commitlint --from=origin/${CI_MERGE_REQUEST_TARGET_BRANCH_NAME} --to=origin/${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME};
```

This example shows `commitlint` only being run on merge request. This is because `commitlint` can only be helpful when bad commits can be fixed before they are merged into master. If a commit is pushed to master that would fail `commitlint`, it still lands in master and has to be reverted. If you decide to remove the restriction to merge requests then you will also have to change the CI variables used, as they are not present in jobs run outside of merge requests.

## Features

### Distribution

The image is based off of the node alpine image.
