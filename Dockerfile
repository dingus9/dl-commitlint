FROM node:8-alpine

RUN npm i -g commitlint \
	&& apk add --no-cache git
